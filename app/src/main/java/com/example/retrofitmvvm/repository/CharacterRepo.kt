package com.example.retrofitmvvm.repository

import com.example.retrofitmvvm.retrofit.Character
import com.example.retrofitmvvm.retrofit.CharacterApi

class CharacterRepo(private val characterApi : CharacterApi ) {

    suspend fun getCharacters() : List<Character>{
        return characterApi.getCharacters()

    }

}