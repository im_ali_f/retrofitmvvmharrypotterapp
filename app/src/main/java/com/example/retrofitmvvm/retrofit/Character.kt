package com.example.retrofitmvvm.retrofit

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true) // ino nafahmidm
data class Character(

    @Json(name = "actor") // chon jaye actor ke to json bod az actor_name estefade shod
    val actor_name : String,

    @Json(name = "image")
    val image : String

)

