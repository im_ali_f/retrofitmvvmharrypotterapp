package com.example.retrofitmvvm.retrofit

import retrofit2.http.GET
// https://hp-api.onrender.com/api/characters
interface CharacterApi {

    @GET("characters")   //END-Point @GET("api/characters") or @GET("characters") we can use both
    suspend fun getCharacters(): List<Character>

}