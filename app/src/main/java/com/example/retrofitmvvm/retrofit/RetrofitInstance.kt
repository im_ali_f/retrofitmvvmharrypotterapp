package com.example.retrofitmvvm.retrofit

import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

// https://hp-api.onrender.com/api/characters


object RetrofitInstance {

    fun provideApi(builder: Retrofit.Builder) : CharacterApi{

        return  builder
            .build()
            .create(CharacterApi::class.java)
    }

    fun ProvideRetrofit(): Retrofit.Builder{

        return Retrofit.Builder()
            .baseUrl("https://hp-api.onrender.com/api/")
            .addConverterFactory(MoshiConverterFactory.create())
    }


}